terraform {
  required_version = "1.1.9"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.35.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.14.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.7.1"
    }
    time = {
      source  = "hashicorp/time"
      version = "0.9.0"
    }
  }
}

data "aws_caller_identity" "current" {} # used for accesing Account ID and ARN
